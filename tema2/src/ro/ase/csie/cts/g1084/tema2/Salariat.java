package ro.ase.csie.cts.g1084.tema2;

import java.util.ArrayList;


public class Salariat implements Pontaj{
	String numeSalariat;
	String CNP;
	int varstaSalariat;
	ArrayList<Float> nrOrePontajSalariat;
	
	public Salariat(String numeSalariat, String CNP, int varstaSalariat, ArrayList<Float> nrOrePontajSalariat) {
		super();
		this.numeSalariat = numeSalariat;
		this.CNP = CNP;
		this.varstaSalariat = varstaSalariat;
		this.nrOrePontajSalariat = nrOrePontajSalariat;
		
	}
	
	
	
	public String getNumeSalariat() {
		return numeSalariat;
	}



	public void setNumeSalariat(String numeSalariat) {
		this.numeSalariat = numeSalariat;
	}



	public String getCNP() {
		return CNP;
	}



	public void setCNP(String cNP) {
		CNP = cNP;
	}



	public int getVarstaSalariat() {
		return varstaSalariat;
	}



	public void setVarstaSalariat(int varstaSalariat) {
		this.varstaSalariat = varstaSalariat;
	}



	public ArrayList<Float> getNrOrePontajSalariat() {
		return nrOrePontajSalariat;
	}



	public void setNrOrePontajSalariat(ArrayList<Float> nrOrePontajSalariat) {
		this.nrOrePontajSalariat = nrOrePontajSalariat;
	}



	public float getMedie() {
		return Pontaj.nrMediuOreLucrate(this.nrOrePontajSalariat);
	}
	
	
	
}
