package ro.ase.csie.cts.g1084.tema2;

import java.util.ArrayList;

public interface Pontaj {
	public static float nrMediuOreLucrate(ArrayList<Float> nrOrePontajSalariat) {
		float suma = 0;
		if(nrOrePontajSalariat.size() > 0) {
			for (float ora : nrOrePontajSalariat) {
				suma += ora;
			}
			return suma / nrOrePontajSalariat.size();
		}
		else {
			return 0;
		}			
	}
}
