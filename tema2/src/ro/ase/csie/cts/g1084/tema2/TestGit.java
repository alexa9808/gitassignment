package ro.ase.csie.cts.g1084.tema2;

import java.util.ArrayList;

public class TestGit {
	public static void main(String[] args) {
		System.out.println("Hello Git ! Denumirea proiectului de licență este [Aplicatie informatica pentru gestiunea departamentului de salarizari intr-o firma]");
		
		
			ArrayList<Float> ore = new ArrayList<>();
			ore.add((float) 10);
			ore.add((float) 22);
			
			Salariat salariat = new Salariat("Popescu", "1701105460023", 40, ore);
			
			float targetLunarMedieOre = 15f;
			
			float medieOreSalariat = salariat.getMedie();
			
			if(targetLunarMedieOre != medieOreSalariat) {
				System.out.println("Din punct de vedere al  mediei de ore lucrate salariatul nu si-a atins target-ul lunar");
			}
			else {
				System.out.println("Target atins ca medie de ore lucrate");
			}
					
		
	}
	
	
	
}
